module yourmodule

go 1.17

require (
    github.com/gin-gonic/gin v1.7.4
    github.com/go-sql-driver/mysql v1.6.0
    github.com/gorilla/mux v1.8.0
)